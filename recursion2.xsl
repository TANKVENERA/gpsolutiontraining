<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs"
    version="1.0">


   
<xsl:template match="/Guests" name="recursion">     
    <xsl:param name="count" select="count(//@* | //Type | //Address)"/>
    <xsl:param name="position" select="'1'"/>
        <xsl:if test="$count>0">
            <xsl:value-of select="(//@* | //Type | //Address)[position()=$position]"/> 
            <xsl:choose>
                <xsl:when test="$position mod 6 = 0 and $position!=count(//@* | //Type | //Address)">
                        <xsl:text>#</xsl:text>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:if test="$position!=count(//@* | //Type | //Address)">
                         <xsl:text>|</xsl:text>
                    </xsl:if>
                </xsl:otherwise>
            </xsl:choose>
            <xsl:call-template name="recursion">
                <xsl:with-param name="count" select="$count - 1"/>
                <xsl:with-param name="position" select="$position + 1"/>
            </xsl:call-template>
        </xsl:if>  
</xsl:template>  
      
</xsl:stylesheet>

