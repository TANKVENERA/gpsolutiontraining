<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:h="HouseChema"  xmlns:i="HouseInfo" xmlns:r="RoomsChema"
    exclude-result-prefixes="r h i" version="1.0" >
    
   
    <xsl:template match="/">
    <AllRooms>
        <xsl:apply-templates/>
    </AllRooms>
    </xsl:template>
    
    <xsl:template match="h:Houses">
        <xsl:for-each select="h:House">
            <xsl:sort   select="@City"/>
            <xsl:call-template name="a"/>
        </xsl:for-each>
    </xsl:template>
    
    <xsl:template name="a">
        <xsl:for-each select=".//h:Block">
            <xsl:sort   select="@number"/>
            <xsl:call-template name="b"/>
        </xsl:for-each>
    </xsl:template>
    
    <xsl:template name="b"> 
        <xsl:variable name="houseRoomsCount" select="count(.//ancestor::h:House//r:Room)"/>
        <xsl:variable name="blockRoomsCount" select="count(.//ancestor::h:Block//r:Room)"/>
        <xsl:variable name="houseGuestsCount" select="sum(.//ancestor::h:House//r:Room/@guests)"/>
        
        <xsl:for-each select=".//r:Room">
            <xsl:sort select="@nuber" data-type="number"/>
            <Room><xsl:text>&#10;</xsl:text>
                <xsl:call-template name="address"/>
                <HouseRoomsCount><xsl:value-of select="$houseRoomsCount"/></HouseRoomsCount><xsl:text>&#10;</xsl:text>
                <BlockRoomsCount><xsl:value-of select="$blockRoomsCount"/></BlockRoomsCount><xsl:text>&#10;</xsl:text>
                <HouseGuestsCount><xsl:value-of select="$houseGuestsCount"/></HouseGuestsCount><xsl:text>&#10;</xsl:text>
                <GuestsPerRoomAverage><xsl:value-of select="floor($houseGuestsCount div $houseRoomsCount)"/></GuestsPerRoomAverage><xsl:text>&#10;</xsl:text>
                <xsl:call-template name="allocated"/>
            </Room><xsl:text>&#10;</xsl:text>
        </xsl:for-each>
    </xsl:template>
    
    <xsl:template name="address">
        <xsl:variable name="smallcase" select="'abcdefghijklmnopqrstuvwxyz'" />
        <xsl:variable name="uppercase" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'" />
        <xsl:variable name="city" select=".//ancestor::h:House/@City"/>
        <xsl:variable name="street" select=".//ancestor::h:House/i:Address"/>
        <xsl:variable name="block" select=".//ancestor::h:Block/@number"/>
        <xsl:variable name="room" select="./@nuber"/>
        <Address><xsl:value-of select="concat(translate($city, $smallcase, $uppercase),'/',$street,'/',$block,'/',$room)"/></Address><xsl:text>&#10;</xsl:text>  
    </xsl:template>
    
    <xsl:template name="allocated">
        <xsl:variable name="amount" select="./@guests"/>
        <xsl:variable name="flag" select="false()"/>
        <xsl:element name="Allocated">
            <xsl:attribute name="Single">
                <xsl:if test="$amount=1">
                    <xsl:value-of select="not($flag)"/>
                </xsl:if>
                <xsl:if test="$amount!=1">
                    <xsl:value-of select="$flag"/>
                </xsl:if>              
            </xsl:attribute>
            <xsl:attribute name="Double">
                <xsl:if test="$amount=2">
                    <xsl:value-of select="not($flag)"/>
                </xsl:if>
                <xsl:if test="$amount!=2">
                    <xsl:value-of select="$flag"/>
                </xsl:if>              
            </xsl:attribute>
            <xsl:attribute name="Triple">
                <xsl:if test="$amount=3">
                    <xsl:value-of select="not($flag)"/>
                </xsl:if>
                <xsl:if test="$amount!=3">
                    <xsl:value-of select="$flag"/>
                </xsl:if>
            </xsl:attribute>
            <xsl:attribute name="Quarter">
                <xsl:if test="$amount=4">
                    <xsl:value-of select="not($flag)"/>
                </xsl:if>
                <xsl:if test="$amount!=4">
                    <xsl:value-of select="$flag"/>
                </xsl:if>
            </xsl:attribute>
        </xsl:element>     
    </xsl:template>
   
</xsl:stylesheet>