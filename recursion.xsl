<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs"
    version="1.0">

<xsl:template match="/ | *">
    <xsl:apply-templates select="*"/>    
</xsl:template>
   
<xsl:template match="Guests/*">  
    <xsl:for-each select="./@* | ./Type | .//Address">
        <xsl:value-of select="."/> 
        <xsl:if test="position()!=last()">
            <xsl:text>|</xsl:text>
        </xsl:if>   
    </xsl:for-each>
    <xsl:if test="position()!=last()">
        <xsl:text>#</xsl:text>
    </xsl:if>
</xsl:template>  
      
</xsl:stylesheet>

