<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs"
    version="1.0">
    <xsl:output method="xml" encoding="UTF-8" indent="yes"/>
 
 <xsl:template match="/">
     <Guests>
        <xsl:call-template name="split">
            <xsl:with-param name="text" select="root/text()"></xsl:with-param>
        </xsl:call-template>
     </Guests>
 </xsl:template>
 
  <xsl:template name="split">
      <xsl:param name="text" />
      <xsl:param name="person" select="substring-before($text, '#')"/>
      <xsl:variable name="lineWithAge" select="substring-after($person, '|')"/>                        
      <xsl:variable name="lineWithNationality" select="substring-after($lineWithAge, '|')"/>
      <xsl:variable name="lineWithGender" select="substring-after($lineWithNationality, '|')"/>
      <xsl:variable name="lineWithName" select="substring-after($lineWithGender, '|')"/>
          <xsl:choose>
              <xsl:when test="contains($text, '#')">
                          <Guest 
                              Age="{substring-before($lineWithAge, '|')}"
                              Nationality="{substring-before($lineWithNationality,  '|')}"
                              Gender="{substring-before($lineWithGender,  '|')}"
                              Name="{substring-before($lineWithName,  '|')}">
                              <Type>
                                  <xsl:value-of select="substring-before($person,  '|')"/>
                              </Type>
                              <Profile>
                                  <Address>
                                      <xsl:value-of select="substring-after($lineWithName,  '|')"/>
                                  </Address>
                              </Profile>
                          </Guest>   
                          <xsl:call-template name="split">
                              <xsl:with-param name="text" select="substring-after($text,'#')"/>
                          </xsl:call-template>    
              </xsl:when>
              <xsl:when test="not(contains($text, '#')) and string-length($text)>0">
                  <xsl:call-template name="split">
                      <xsl:with-param name="text" select="concat($text,'#')"/>
                  </xsl:call-template>    
              </xsl:when>
          </xsl:choose>   
 </xsl:template>
 
</xsl:stylesheet>