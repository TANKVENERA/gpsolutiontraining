<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    version="1.0">
<xsl:output method="xml" encoding="UTF-8" indent="yes" />
    
    <xsl:key name="items-by-name" match="item" use="substring(@Name,1,1)"/>
    
    
 <xsl:template match="/list">
    
     <xsl:for-each select="item[generate-id(.)=generate-id(key('items-by-name',substring(@Name,1,1)))]">
         <xsl:sort select="@Name"/>
         <capital value="{substring(@Name,1,1)}"><xsl:text>&#10;</xsl:text>
             <xsl:for-each select="key('items-by-name', substring(@Name,1,1))">
                 <xsl:sort select="@Name"/>
             <name> 
             <xsl:value-of select="@Name"/>
             </name> 
         </xsl:for-each>
         </capital><xsl:text>&#10;</xsl:text>  
         </xsl:for-each>
         
         
    
     
 </xsl:template>   
    
</xsl:stylesheet>