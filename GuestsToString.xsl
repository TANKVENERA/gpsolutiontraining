<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs"
    version="1.0">
    
    <xsl:template match="Guests">
        <xsl:apply-templates select="Guest"/>
    </xsl:template>
    
    <xsl:template match="Guest">
        <xsl:variable name="type" select="./Type"/>
        <xsl:variable name="name" select="@Name"/>
        <xsl:variable name="age" select="@Age"/>
        <xsl:variable name="nationality" select="@Nationalty"/>
        <xsl:variable name="gender" select="@Gender"/>
        <xsl:variable name="profile" select=".//Profile/Address"/>
        <xsl:value-of select="concat($type,'|',$age,'|',$nationality,'|',$gender,'|',$name,'|',$profile)" ></xsl:value-of>
        <xsl:if test="position()!=last()">
            <xsl:text>#</xsl:text>
        </xsl:if>
    </xsl:template>
    
</xsl:stylesheet>