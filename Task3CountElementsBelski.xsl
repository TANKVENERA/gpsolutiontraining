<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    version="1.0">
    
    <xsl:key name="elements" match="* | @*" use="name()"/>

    <xsl:template match="/">
        <xsl:for-each select="(//* | //@*)[generate-id(.)=generate-id(key('elements',name()))]">
            <xsl:text>Node '</xsl:text><xsl:value-of select="name()"/>
                <xsl:text>' found </xsl:text><xsl:value-of select="count(key('elements',name()))"/>
                <xsl:text> times</xsl:text><xsl:text>&#10;</xsl:text>
            </xsl:for-each>
    </xsl:template>
</xsl:stylesheet>

<!--//* | //@*)[count(. | key('elements', name())[1]) = 1]-->

<!--[generate-id(.)=generate-id(key('elements',name()))]-->