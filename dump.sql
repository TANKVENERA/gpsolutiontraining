-- MySQL dump 10.13  Distrib 5.7.18, for Win64 (x86_64)
--
-- Host: localhost    Database: gp
-- ------------------------------------------------------
-- Server version	5.7.18-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `gpt_hotel`
--

DROP TABLE IF EXISTS `gpt_hotel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gpt_hotel` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `CODE` varchar(32) NOT NULL,
  `CITY_ID` bigint(20) NOT NULL,
  `COUNTRY_ID` int(11) DEFAULT NULL,
  `CHAIN_ID` bigint(20) DEFAULT NULL,
  `PHONE` varchar(100) NOT NULL,
  `FAX` varchar(50) NOT NULL,
  `URL` varchar(255) NOT NULL,
  `CHECK_IN` time DEFAULT NULL,
  `CHECK_OUT` time DEFAULT NULL,
  `EMAIL` varchar(255) DEFAULT NULL,
  `ZIP` varchar(50) DEFAULT NULL,
  `LATITUDE` decimal(10,6) NOT NULL,
  `LONGITUDE` decimal(10,6) NOT NULL,
  `ACTIVE` bit(1) DEFAULT NULL,
  `DEFAULT_IMAGE` bigint(20) DEFAULT NULL,
  `CREATED` datetime DEFAULT NULL,
  `CATEGORY_ID` enum('HOTEL','HOSTEL','APARTMENTS') DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `CITY_ID` (`CITY_ID`),
  CONSTRAINT `gpt_hotel_ibfk_1` FOREIGN KEY (`CITY_ID`) REFERENCES `gpt_location` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gpt_hotel`
--

LOCK TABLES `gpt_hotel` WRITE;
/*!40000 ALTER TABLE `gpt_hotel` DISABLE KEYS */;
INSERT INTO `gpt_hotel` VALUES (1,'####',4,3,NULL,'111-000','333-999','/33/SS',NULL,NULL,NULL,NULL,12.334000,13.444000,NULL,NULL,NULL,'HOTEL'),(2,'AA',5,2,NULL,'11-11','22-000','/ST/ST',NULL,NULL,NULL,NULL,1034.103400,1134.113400,'',NULL,NULL,'HOSTEL'),(3,'DFWEF',4,3,NULL,'11-11','22-22','/W/W',NULL,NULL,NULL,NULL,10.100000,11.110000,'',NULL,NULL,'HOSTEL'),(4,'DFWEF',6,3,NULL,'11-11','22-22','/W/W',NULL,NULL,NULL,NULL,10.100000,11.110000,'',NULL,NULL,'HOTEL');
/*!40000 ALTER TABLE `gpt_hotel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gpt_hotel_image_type`
--

DROP TABLE IF EXISTS `gpt_hotel_image_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gpt_hotel_image_type` (
  `ID` tinyint(3) NOT NULL AUTO_INCREMENT,
  `TYPE_NAME` varchar(128) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gpt_hotel_image_type`
--

LOCK TABLES `gpt_hotel_image_type` WRITE;
/*!40000 ALTER TABLE `gpt_hotel_image_type` DISABLE KEYS */;
INSERT INTO `gpt_hotel_image_type` VALUES (1,'GENERAL'),(2,'ROOM'),(3,'LOBBY'),(4,'PARKING');
/*!40000 ALTER TABLE `gpt_hotel_image_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gpt_hotel_images`
--

DROP TABLE IF EXISTS `gpt_hotel_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gpt_hotel_images` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `HOTEL_ID` bigint(20) NOT NULL,
  `TYPE_ID` tinyint(3) NOT NULL,
  `URL` varchar(600) NOT NULL,
  `THUMNAIL_URL` varchar(600) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `HOTEL_ID` (`HOTEL_ID`),
  KEY `TYPE_ID` (`TYPE_ID`),
  CONSTRAINT `gpt_hotel_images_ibfk_1` FOREIGN KEY (`HOTEL_ID`) REFERENCES `gpt_hotel` (`ID`),
  CONSTRAINT `gpt_hotel_images_ibfk_2` FOREIGN KEY (`TYPE_ID`) REFERENCES `gpt_hotel_image_type` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gpt_hotel_images`
--

LOCK TABLES `gpt_hotel_images` WRITE;
/*!40000 ALTER TABLE `gpt_hotel_images` DISABLE KEYS */;
INSERT INTO `gpt_hotel_images` VALUES (1,1,1,'/###','/$$$'),(2,2,1,'/!!!','/@@@'),(3,3,1,'/%%%','/***'),(4,4,1,'/HILTON/MINSK/1','/$$$'),(5,4,1,'/HILTON/MINSK/2','$$$'),(6,4,1,'/HILTON/MINSK/3','/$$$/'),(7,4,1,'/HILTON/MINSK/4','$$$');
/*!40000 ALTER TABLE `gpt_hotel_images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gpt_hotel_name`
--

DROP TABLE IF EXISTS `gpt_hotel_name`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gpt_hotel_name` (
  `HOTEL_ID` bigint(20) NOT NULL,
  `LANG_ID` tinyint(3) NOT NULL,
  `NAME` varchar(500) NOT NULL,
  PRIMARY KEY (`HOTEL_ID`,`LANG_ID`),
  KEY `LANG_ID` (`LANG_ID`),
  CONSTRAINT `gpt_hotel_name_ibfk_1` FOREIGN KEY (`HOTEL_ID`) REFERENCES `gpt_hotel` (`ID`),
  CONSTRAINT `gpt_hotel_name_ibfk_2` FOREIGN KEY (`LANG_ID`) REFERENCES `gpt_language` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gpt_hotel_name`
--

LOCK TABLES `gpt_hotel_name` WRITE;
/*!40000 ALTER TABLE `gpt_hotel_name` DISABLE KEYS */;
INSERT INTO `gpt_hotel_name` VALUES (1,1,'ORBITA'),(2,1,'HOTEL_NEVSKI'),(3,1,'HAMPTON_BY_HILTON'),(4,2,'ХЭМПТОН-ХИЛТОН');
/*!40000 ALTER TABLE `gpt_hotel_name` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gpt_language`
--

DROP TABLE IF EXISTS `gpt_language`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gpt_language` (
  `ID` tinyint(3) NOT NULL AUTO_INCREMENT,
  `code` varchar(2) NOT NULL,
  `NAME` varchar(40) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gpt_language`
--

LOCK TABLES `gpt_language` WRITE;
/*!40000 ALTER TABLE `gpt_language` DISABLE KEYS */;
INSERT INTO `gpt_language` VALUES (1,'EN','ENGLISH'),(2,'RU','RUSSIAN'),(3,'BY','BELARUSSIAN');
/*!40000 ALTER TABLE `gpt_language` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gpt_location`
--

DROP TABLE IF EXISTS `gpt_location`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gpt_location` (
  `ID` bigint(32) NOT NULL AUTO_INCREMENT,
  `code` varchar(32) NOT NULL,
  `TYPE_ID` tinyint(3) NOT NULL,
  `PARENT` bigint(20) NOT NULL,
  `active` bit(1) NOT NULL,
  `latitude` decimal(10,6) NOT NULL,
  `longitude` decimal(10,6) NOT NULL,
  `TIME_ZONE` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `TYPE_ID` (`TYPE_ID`),
  CONSTRAINT `gpt_location_ibfk_1` FOREIGN KEY (`TYPE_ID`) REFERENCES `gpt_location_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gpt_location`
--

LOCK TABLES `gpt_location` WRITE;
/*!40000 ALTER TABLE `gpt_location` DISABLE KEYS */;
INSERT INTO `gpt_location` VALUES (1,'#########',1,0,'',23.345230,45.345000,NULL),(2,'%%%%%%%%%%%%',2,1,'',33.111000,22.444000,NULL),(3,'@@@@@@@@@@',2,1,'',444.444000,555.666000,NULL),(4,'*$%&^*&*',3,3,'',433.444000,3435.006000,NULL),(5,'###',3,2,'',90.000000,80.000000,NULL),(6,'###',3,3,'',20.000000,30.000000,NULL),(7,'###',2,1,'',20.000000,30.000000,NULL),(8,'###',2,1,'',20.000000,30.000000,NULL),(9,'###',2,1,'',20.000000,30.000000,NULL);
/*!40000 ALTER TABLE `gpt_location` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gpt_location_name`
--

DROP TABLE IF EXISTS `gpt_location_name`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gpt_location_name` (
  `LOCATION_ID` bigint(20) NOT NULL,
  `LANG_ID` tinyint(3) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`LOCATION_ID`,`LANG_ID`),
  KEY `LANG_ID` (`LANG_ID`),
  CONSTRAINT `gpt_location_name_ibfk_1` FOREIGN KEY (`LOCATION_ID`) REFERENCES `gpt_location` (`ID`),
  CONSTRAINT `gpt_location_name_ibfk_2` FOREIGN KEY (`LANG_ID`) REFERENCES `gpt_language` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gpt_location_name`
--

LOCK TABLES `gpt_location_name` WRITE;
/*!40000 ALTER TABLE `gpt_location_name` DISABLE KEYS */;
INSERT INTO `gpt_location_name` VALUES (1,1,'ROOT'),(2,1,'RUSSIA'),(3,1,'BELARUS'),(4,1,'MINSK'),(5,1,'ST_PITERBURG'),(6,2,'МИНСК'),(7,2,'РОССИЯ'),(8,2,'БЕЛАРУСЬ'),(9,2,'США');
/*!40000 ALTER TABLE `gpt_location_name` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gpt_location_type`
--

DROP TABLE IF EXISTS `gpt_location_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gpt_location_type` (
  `id` tinyint(4) NOT NULL AUTO_INCREMENT,
  `TYPE_NAME` varchar(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gpt_location_type`
--

LOCK TABLES `gpt_location_type` WRITE;
/*!40000 ALTER TABLE `gpt_location_type` DISABLE KEYS */;
INSERT INTO `gpt_location_type` VALUES (1,'ROOT'),(2,'COUNTRY'),(3,'CITY');
/*!40000 ALTER TABLE `gpt_location_type` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-10-30 16:06:08
