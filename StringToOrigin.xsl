<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:str="http://exslt.org/strings"
    exclude-result-prefixes="str"
    version="1.0">
    
    <xsl:template match="line">
        <xsl:variable name="token" select="str:split(., '#')"/>
        <Guests><xsl:text>&#10;</xsl:text>
            <xsl:for-each select="$token">
                <Guest>
                <xsl:call-template name="person"/>
            </Guest><xsl:text>&#10;</xsl:text>
            </xsl:for-each>
        </Guests>
    </xsl:template>
    
    <xsl:template name="person">
        <xsl:variable name="person" select="str:split(., '|')"/>
        <xsl:attribute name="Age">
            <xsl:value-of select="$person[2]"/>
        </xsl:attribute>
        <xsl:attribute name="Nationality">
            <xsl:value-of select="$person[3]"/>
        </xsl:attribute>
        <xsl:attribute name="Gender">
            <xsl:value-of select="$person[4]"/>
        </xsl:attribute>
        <xsl:attribute name="Name">
            <xsl:value-of select="$person[5]"/>
        </xsl:attribute>
        <xsl:text>&#10;</xsl:text>
        <xsl:element name="Type">
            <xsl:value-of select="$person[1]"/>
        </xsl:element><xsl:text>&#10;</xsl:text>
        <xsl:element name="Profile"><xsl:text>&#10;</xsl:text>
            <xsl:element name="Address">
                <xsl:value-of select="$person[6]"/>
            </xsl:element><xsl:text>&#10;</xsl:text>
        </xsl:element><xsl:text>&#10;</xsl:text>
    </xsl:template>
    
</xsl:stylesheet>